#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'packer.exe' : './packer';
const command = resolve(__dirname, '..', 'tools', execName);
const packer = spawn(command, process.argv.slice(2), { cwd: process.cwd() });

packer.stdout.pipe(process.stdout);
packer.stderr.pipe(process.stderr);
packer.on('error', function(err) {
  console.error(`Received an error while executing the Packer binary: ${err}`);
  process.exit(1);
});
